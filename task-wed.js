class Vehicle {
    constructor(wheel, country) {
        this.wheel = wheel;
        this.country = country;
    }

    info() {
        console.log(`Jenis Kendaraan roda ${this.wheel} dari negara ${this.country}`);
    }
}

class Car extends Vehicle {
    constructor(wheel, country, brand, price, taxPercent) {
        super(wheel, country);
        this.brand = brand;
        this.price = price;
        this.taxPercent = taxPercent;
    }

    totalPrice() {
        return (1 + this.taxPercent) * this.price;
    }

    info() {
        super.info();
        console.log(`Kendaraan ini nama mereknya ${this.brand} dengan total harga Rp.${this.totalPrice()}`);
    }
}

//Instance 
const vehicle1 = new Vehicle(4, 'Italia');
vehicle1.info();
const vehicle2 = new Vehicle(4, 'USA');
vehicle2.info();

const car = new Car(4, 'Jepang', 'Toyota', 250000000, 0.11);
car.info();