const studentsData = [{
        nama: "Fadli",
        provinsi: "Jawa Tengah",
        umur: 20,
        status: "single",
    },
    {
        nama: "Ikhwan",
        provinsi: "Jakarta",
        umur: 23,
        status: "punya pacar",
    },
    {
        nama: "Faris",
        provinsi: "Jawa Tengah",
        umur: 20,
        status: "single",
    },
    {
        nama: "Aji",
        provinsi: "Jawa Barat",
        umur: 21,
        status: "single",
    },
    {
        nama: "Andre",
        provinsi: "Jawa Barat",
        umur: 21,
        status: "single",
    },
]

const isJabar = function(indeks, data) {
    if (data[indeks].provinsi == "Jawa Barat") {
        return "Jawa Barat";
    } else {
        return data[indeks].provinsi;
    }

}

const isGraterTwentyTwo = function(indeks, data) {
    if (data[indeks].umur > 22) {
        return "diatas 22";
    } else {
        return "dibawah 22";
    }
}

const isSingle = function(indeks, data) {
    if (data[indeks].status == 'single') {
        return "single";
    } else {
        return data[indeks].status;
    }
}

const printAll = (data, callback) => {
    for (let i = 0; i < data.length; i++) {
        callback(data[i].nama, isJabar(i, data), isGraterTwentyTwo(i, data), isSingle(i, data));
    }
}

printAll(studentsData, (nama, provinsi, umur, status) => {
    console.log(`Nama saya ${nama}, saya tinggal di ${provinsi}, umur saya ${umur} tahun. Status saya ${status} loh`);
})